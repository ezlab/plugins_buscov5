
rule hr_table:
    input:  config["out_path"] + "/" + config["dataset_name"] + "_summary.txt",
    output: config["out_path"] + "/" + config["dataset_name"] + "_summary_hr.txt"
    shell:  "column -t {input} > {output}"

