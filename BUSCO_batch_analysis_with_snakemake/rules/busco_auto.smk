#---- Run BUSCO in autolineage mode

rule busco_auto:
    input:
        bins = config['data_path'] + "/{sample}" + config['file_suff']
    output:
        res_fold = directory(config["out_path"] + "/run_{sample}")
    params:
        download_base_url = config["download_base_url"],
        download_path = config["download_path"],
        out_path = config["out_path"],
        config_ini = config["config_ini"],
        busco =  config["busco"],
        autolineage = config["autolineage"]
    conda:
        config["conda_env"]
    benchmark:
        "benchmarks/busco/{sample}.tsv"
    threads: config["BUSCO_threads"]
    shell:
        """
        set -x
        set -e

        {params.busco} --offline --download_base_url {params.download_base_url} \
        --download_path {params.download_path} \
        {params.autolineage} -i {input.bins} -o run_{wildcards.sample} \
        -m geno -c {threads} \
        --out_path {params.out_path} \
        --config {params.config_ini} \
        || true; \
        ls {output.res_fold}/short_summary.specific* \
        || touch {output.res_fold}/short_summary.specific.txt
        """

