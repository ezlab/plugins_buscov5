#---- Parse the results

rule parse_res:
    input:
        expand(config["out_path"] + "/run_{sample}", sample=SAMPLES)
    output:
        config["out_path"] + "/" + config["dataset_name"] + "_summary.txt"
    params:
        out_path = config["out_path"],
        autolineage = config["autolineage"]
    run:
        import re
        import os
        with open (output[0], 'w') as out:
            header=("Sample_name\tDataset\tComplete\tSingle\tDuplicated\tFragmented\tMissing\tn_markers\t")
            if params[1]=="--auto-lineage":
                header= header + "Scores_bacteria_odb10" + "\t" + "Scores_archaea_odb10" + "\t" + "Scores_eukaryota_odb10" + "\n"
            elif params[1]=="--auto-lineage-euk":
                header= header + "Scores_bacteria_odb10" + "\t" + "Scores_eukaryota_odb10" + "\n"
            elif params[1]=="--auto-lineage-prok":
                header= header + "Scores_bacteria_odb10" + "\t" + "Scores_archaea_odb10" + "\n"

            out.write(header)

        for infile in input:
            short_summary = str(glob.glob(infile + '/short_summary.spec*.txt')[0])

            if os.stat(short_summary).st_size == 0:
                dataset="Check_logs"
                filename=re.sub('run_', '', str(infile))
                scores= "-\t"*8 + "-"

                with open (output[0], 'a') as out:
                   out.write(str(filename) + "\t" + str(dataset) + "\t" + str(scores) + "\n")

            else:
                with open (short_summary, 'rt') as file:
                    lines = [x.rstrip() for x in file]
                    dataset = re.sub(' \(.+', '', re.sub('.+is: ', '', str(lines[1])))
                    filename = re.sub('run_', '', str(infile))
                    scores = str(re.sub('/t', '', str(lines[8])))
                    scores_sep = scores.translate(str.maketrans({'%': '\t', ' ': '', 'C': '', 'D':'', 'S': '',
                                                                 'F': '', 'M': '', 'n': '', ':': '', ']': '',
                                                                 '[': '', ',': ''}))

            # retrieve scores of 'root' datasets
                scores_domains = {}
                domains=[]
                #if os.path.isdir((str(infile) + '/auto_lineage/run_' + domain + '_odb10')):
                if params[1]=="--auto-lineage":
                    domains= domains + ['bacteria', 'eukaryota', 'archaea']
                elif params[1]=="--auto-lineage-euk":
                    domains= domains + ['eukaryota']
                elif params[1]=="--auto-lineage-prok":
                    domains= domains + ['bacteria', 'archaea']
                for domain in domains:
                    if os.path.isdir((str(infile) + '/auto_lineage/run_' + domain + '_odb10')):

                        with open (str(infile) + '/auto_lineage/run_' + domain + '_odb10/short_summary.txt', 'rt') as file:
                            lines = [x.rstrip() for x in file]
                            scores = str(lines[8])
                            scores_domains[domain] = scores

                    else:
                        with open (str(infile) + '/run_' + domain + '_odb10/short_summary.txt', 'rt') as file:
                            lines = [x.rstrip() for x in file]
                            scores = str(lines[8])
                            scores_domains[domain] = scores
                
                if params[1]=="--auto-lineage":
                    with open (output[0], 'a') as out:
                        out.write(str(filename) + "\t" + str(dataset) + str(scores_sep)
                                  + scores_domains.get('bacteria','-')
                                  + scores_domains.get('archaea','-')
                                  + scores_domains.get('eukaryota','-') + "\n")

                elif params[1]=="--auto-lineage-euk":
                    with open (output[0], 'a') as out:
                        out.write(str(filename) + "\t" + str(dataset) + str(scores_sep)
                                  + scores_domains.get('eukaryota','-') + "\n")

                elif params[1]=="--auto-lineage-prok":
                    with open (output[0], 'a') as out:
                        out.write(str(filename) + "\t" + str(dataset) + str(scores_sep)
                                  + scores_domains.get('bacteria','-')
                                  + scores_domains.get('archaea','-') + "\n")


