# busco batch analysis through snakemake
A snakemake workflow to run BUSCO on a collection of genomes (e.g. bins from metagenomic data).
The workflow expects you provide a folder with multiple genomes to be analyzed. 

Disclaimer: The workflow might be subject to changes, e.g. with integration of other informative metrics.

## Snakemake workflow usage:

### Note: You need to have all the BUSCO datasets and files already downloaded on your machine
To run smoothly, these BUSCO analyses are run offline (`--offline` flag), i.e. BUSCO will not attempt to download files. This is necessary to avoid possible race conditions.   

1. Download all the datasets. BUSCO 5.2.0 has an option for bulk download, you can run: \
`busco  --download_base_url "https://busco-data.ezlab.org/v5/data/"  --download_path /path/where/to/store/the/files --download keyword`
where keyword can be: \
‘all’ to download all the datasets  \
‘prokaryota’ for prok datasets only  \
‘eukaryota’ for euk datasets only \
'virus' for virus datasets \
Note: at the end of the download you’ll get a BuscoLogger error, don’t mind, it’s a small bug that doesn’t affect the integrity of the datasets.
2. If not already, install conda and Snakemake (https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)
3. Clone the repository: `git clone https://gitlab.com/ezlab/plugins_buscov5.git` and `cd plugins_BUSCOV5/BUSCO_batch_analysis_with_snakemake`
4. Check the config/config.yaml file, change the parameters (e.g. input path, threads) according to your needs.
5. From the `BUSCO_batch_analysis_with_snakemake` directory run: \
`snakemake -s Snakefile --cores 30 --use-conda`, or without the `--use-conda` flag if you want to use an existing BUSCO installation. \
You can also submit the job to a cluster using a command like: \
`snakemake -s Snakefile --printshellcmds --rerun-incomplete --cluster "sbatch --ntasks-per-node=1 --cpus-per-task=5 --job-name=job_name --partition=partition_name" --jobs 6` \
Currently, running BUSCO through a workflow management system can considerably reduce the overall runtime when assessing metagenomic bins. E.g. using  5 CPUs per BUSCO analysis (specified in the `config/config.yaml` file), works best when assessing prokaryotic or small eukaryotic genomes in batch with this Snakemake workflow.

Along with the standard BUSCO output, you'll obtain a summary table \*_summary.txt (and a human-readable table \*_summary_hr.txt) that looks like this:
```
Sample_name              Dataset                Complete  Single  Duplicated  Fragmented  Missing  n_markers  Scores_bacteria_odb10                        Scores_archaea_odb10                          Scores_eukaryota_odb10
results/GCF_000845105.1  aviadenovirus_odb10    100.0     100.0   0.0         0.0         0.0      13         C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:124  C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:194   C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:255
results/GCF_003999355.1  lactobacillales_odb10  98.8      98.3    0.5         0.7         0.5      402        C:92.7%[S:91.9%,D:0.8%],F:2.4%,M:4.9%,n:124  C:15.4%[S:14.4%,D:1.0%],F:3.6%,M:81.0%,n:194  C:2.0%[S:1.6%,D:0.4%],F:1.2%,M:96.8%,n:255
results/GCF_000847945.1  varicellovirus_odb10   97.3      91.9    5.4         2.7         0.0      37         C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:124  C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:194   C:0.0%[S:0.0%,D:0.0%],F:0.0%,M:100.0%,n:255
results/no_genes         Check_logs             -         -       -           -           -        -          -                                            -                                             -
```
Where "Dataset" is the dataset automatically picked by BUSCO to evaluate the input file. The "Complete, Single,  Duplicated,  Fragmented,  Missing,  n_markers" columns refer to this dataset. Additionally, the last three columns report the scores for the three 'root' datasets (bacteria_odb10, archeae_odb10 and eukaryota_odb10). These might be helpful to spot cross-domain contaminations. However, depending on the input, there can be a certain level of cross-domain matches. Therefore, scoring in multiple 'root' datasets does not necessarely means the presence of contaminant species. If no genes were found by BUSCO or the run crashed, a "Check_logs" is returned in the summary table.
